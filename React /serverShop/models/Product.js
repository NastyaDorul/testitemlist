const { Schema, model, Types } = require("mongoose");

const productSchema = new Schema({
  name: { type: String, required: true, minlength: 2, unique: true },
  description: { type: String, required: true, unique: true, timestamps: true },
  price: { type: Number, required: true },
  id: { type: Number, required: true, min: 0, unique: true },
  versionKey: false //все равно создал _v

  //category: { type: Types.objectId, ref: "Category", timestamps: true }, //не понимаю как работать с категориями и id
  //images: { type: Array, required: true },
  // links: [{ type: Types.objectId, ref: "Link" }] //Link это коллекция
});

module.exports = model("Product", productSchema);
