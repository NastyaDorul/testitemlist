const { Schema, model, Types } = require("mongoose");

const gamingSchema = new Schema({
  name: { type: String, required: true },
  nestedItems: [{ type: Schema.Types.ObjectId, ref: "NestedItem" }],
  versionKey: false //все равно создалась версия _v
});

const nestedItemsSchema = new Schema({
  title: { type: String, required: true },
  price: { type: Number, required: true },
  availability: { type: String, required: true },
  description: { type: String },
  brand: { type: String },
  parentCategory: [{ type: Schema.Types.ObjectId, ref: "Gaming" }],
  image: { type: Object }, //?
  versionKey: false //все равно создалась версия _v
});

const Gaming = model("Gaming", gamingSchema);
const NestedItem = model("NestedItem", nestedItemsSchema);

module.exports.Gaming = Gaming;
module.exports.NestedItem = NestedItem;
