const { Router } = require("express");
//const { Schema, model, Types } = require("mongoose");
const Product = require("../models/Product");
const { NestedItem, Gaming } = require("../models/Gaming");
const router = Router();

//-----------------------По-Гет-запросу----добавляю данные в БД-------------------------------------------------------

router.get("/category/electronics/gaming", function(req, res) {
  const gaming = new Gaming({
    name: "Gaming" //тут хотела аналогично прописать вложенные  items, но они определены ниже, поэтому undefined
    //nestedItems: [
    //mySpanishCoach._id,
    //SegaBassFishing._id,
    //MidwayCruisn._id,
    //PipeMania._id
    //]
  });

  gaming.save(function(err) {
    if (err) return console.log(err);

    const mySpanishCoach = new NestedItem({
      title: " My Spanish Coach (for Sony PSP)",
      price: 49.99,
      availability: "In Stock",
      description: "Learn Spanish in a fun and interactive way",
      brand: "No",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwa6522080/images/large/ubi-soft-my-spanish-coach-psp.jpg"
    });

    const SegaBassFishing = new NestedItem({
      title: "Sega Bass Fishing (for Wii)",
      price: 39.99,
      availability: "In Stock",
      description:
        "The sun is out, the fish are biting and your tackle box is filled with 20 different types of lures. With content created exclusively for the Wii, you'll motor out, find your favorite fishing hole, cast off using the Wii Remote? and snag one of four types of bass. When you hook a fighter, your motion sensor will let you know, so hang on and enjoy the ride!",
      brand: "No",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dw1c2b6867/images/large/sega-bass-fishing-wii.jpg"
    });

    const MidwayCruisn = new NestedItem({
      title: "Midway Cruis'n (for Wii)",
      price: 49.99,
      availability: "In Stock",
      description:
        "An all new version of the classic Midway arcade racer, Cruis'n is coming exclusively to the Wii. Choose your ride from an impressive lineup of licensed cars and experience a rush of adrenaline as you race opponents through twelve different street circuits using the Wii Remote™ to steer and perform outrageous stunts. Win races and earn upgrades including: turbos, body kits, neon and nitrous, allowing you to create the ultimate ride.",
      brand: "Midway",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwb06cd931/images/large/midway-cruisn-wii.jpg"
    });

    const PipeMania = new NestedItem({
      title: "Pipe Mania (for Sony PSP)",
      price: 49.99,
      availability: "In Stock",
      description:
        "Pipemania is a fast-paced puzzle game in which players lay down a pre-ordained set of pipes on a tiled grid in order to keep the Flooze, a constant flowing substance that speeds up with progression, moving through the pipes as long as possible without overflowing. Players will need to be fast with their fingers and utilize quick and forward thinking, precise coordination, and keen awareness if they plan to survive the time constraints, pressure and accuracy needed to navigate through the game. There is no chance for players to let their guard down, as complication increases with new elements as players progress through the game.",
      brand: "Atari",
      parentCategory: gaming._id,
      image:
        "http://demandware.edgesuite.net/aaia_prd/on/demandware.static/-/Sites-electronics-catalog/default/dwc06eee61/images/large/atari-pipe-mania-psp.jpg"
    });

    mySpanishCoach.save(function(err) {
      if (err) return console.log(err);
    });
    SegaBassFishing.save(function(err) {
      if (err) return console.log(err);
    });
    MidwayCruisn.save(function(err) {
      if (err) return console.log(err);
    });
    PipeMania.save(function(err) {
      if (err) return console.log(err);
    });

    res.send("We create this obj");
  });
});

//-----------------------По-Гет-запросу----отображение всех данных из коллекции Product в БД-----------------------
router.get("/all-products", function(req, res) {
  Product.find(
    {
      //name: "Telephone",
      //description: "the most popular OPPO A5",
      //price: 4999,
      //id: 123456
    },
    function(err, doc) {
      if (err) return console.log(err);
      console.log("We found this obj", doc);
      res.send(doc);
    }
  );
});

//-----------------------По-Гет-запросу----отображение данных из коллекции Product по ID -----------------------

router.get("/search-byId/:id", function(req, res) {
  const id = req.params.id;
  Product.findOne(
    { id: id }, //_id поиск по ид БД { _id: id }
    function(err, doc) {
      if (err) return console.log(err);
      console.log(`We found this obj ${id}`);
      res.send(doc);
    }
  );
});

//-----------------------По-Гет-запросу----отображение данных из коллекции Product по значению поля "name" ---

router.get("/search-byName/:name", function(req, res) {
  const productName = req.params.name;
  const result = Product.findOne(
    { name: productName }, //_id поиск по ид БД
    function(err, doc) {
      if (err) return console.log(err);
      console.log(`We found this obj ${productName}`);
      res.send(doc);
    }
  );
  //const a = result.sort({ price: -1 });проверить, что такое Querry
  //console.log(result);
});

//-----------------------По-Гет-запросу--поиск с 2-мя парамеррами----------------------------------------------

router.get("/categories/:categoryId/products/:productId", function(req, res) {
  let categoryId = req.params.categoryId;
  let productId = req.params.productId;
  console.log("Without implementation in DB");
  res.send(`Результат поиска в Категории: ${categoryId}  Товар: ${productId}`);
});

module.exports = router;
