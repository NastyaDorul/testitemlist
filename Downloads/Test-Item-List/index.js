const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const fs = require("fs")
const cors = require("cors");
const bodyParser = require("body-parser");
const routes = require("./router");
const app  = express();
const mongoUrl = 'mongodb+srv://admin:admin@cluster0-5ngpq.mongodb.net/test?retryWrites=true&w=majority';
app.use(cors());
app.use(bodyParser.json());
Product = require("./schema/products")


app.use(express.static(__dirname + "/public"));

mongoose.connect(mongoUrl,(err)=> {
    routes(app);
    if (err) throw err;
    app.listen(8080,()=> {
        console.log("Done");
    })
});

product = new Product();
// product.save(function(err){
//     mongoose.disconnect();
//
//     if(err) return console.log(err);
//
//     console.log("Сохранен объект user", product);
// });