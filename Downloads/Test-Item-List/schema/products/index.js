const mongoose = require('mongoose');

const productShema = mongoose.Schema({
    src : String,
    name : {
        type : String,
        default : "SomeProduct"
    },
    coast : {
        type : Number,
        default : 0,
    },
    description : {
        type : String,
        default : "Bla-bla-bla Bla-bla bla-Bla Bla-Bla-bla bla-bla-bla"
    },
    created: {
        type: Date,
        default: Date.now
    }
});

const Image = mongoose.model("Image",productShema);
module.exports = Image;